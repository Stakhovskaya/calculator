﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator_Viola
{
    public class Calculator
    {
        public Calculator() { }
        public double add(double number1, double number2) { return number1 + number2; }
        public double divide(double number1, double number2) { return number1 / number2; }
        public double minus(double number1, double number2) { return number1 - number2; }
        public double multiply(double number1, double number2) { return number1 * number2; }
        public double[,] multiplyMatr(double[,] arr_1, double[,] arr_2, int row_1, int col_1, int col_2)
        {
            double[,] Mass = new double[row_1, col_2];
            double sum = 0;
            for (int i = 0; i < row_1; i++) 
            {
                for (int j = 0; j < col_2; j++)
                {
                    for (int r = 0; r < col_1; r++)
                    {
                        sum += arr_1[i, r] * arr_2[r, j];  
                    }
                    Mass[i, j] = sum;
                    sum = 0;
                }
            }
            return Mass;
        }
    }
}