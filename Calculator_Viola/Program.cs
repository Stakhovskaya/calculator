﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator_Viola
{
    public class Program
    {
        public static void Main(string[] args)
        {

            while (true) 
            {
                Console.WriteLine("Enter your choice:");
                Console.WriteLine("\t 1 - work with numbers");
                Console.WriteLine("\t 2 - work with matrics");
                Console.WriteLine("\t 0 - exit");
                int b = Convert.ToInt32(Console.ReadLine());
                switch (b)
                {

                    case 1:
                        bool flag = true;
                        while (flag)
                        {
                            Console.WriteLine("Enter your choice:");
                            Console.WriteLine("\t 1 - add");
                            Console.WriteLine("\t 2 - divide");
                            Console.WriteLine("\t 3 - minus");
                            Console.WriteLine("\t 4 - multiply");
                            Console.WriteLine("\t 0 - back");
                            int c = Convert.ToInt32(Console.ReadLine());
                            double number_1 = 0;
                            double number_2 = 0;
                            if (c != 0)
                            {
                                number_1 = enterNumber();
                                number_2 = enterNumber();
                            }
                            Calculator calculator = new Calculator();
                            double result = 0;
                            switch (c) 
                            {
                                case 0: flag = false;break;
                                case 1:
                                    result = calculator.add(number_1, number_2);
                                    Console.WriteLine("Your result = " + result);
                                    break;
                                case 2:
                                    result = calculator.divide(number_1, number_2);
                                    Console.WriteLine("Your result = " + result);
                                    break;
                                case 3:
                                    result = calculator.minus(number_1, number_2);
                                    Console.WriteLine("Your result = " + result);
                                    break;
                                case 4:
                                    result = calculator.multiply(number_1, number_2);
                                    Console.WriteLine("Your result = " + result);
                                    break;
                            }
                        }
                        break;
                    case 2:
                        bool flag1 = true;
                        while (flag1)
                        {
                            Console.WriteLine("Enter your choice:");
                            Console.WriteLine("\t 1 - add");
                            Console.WriteLine("\t 0 - back");
                      
                            int d = Convert.ToInt32(Console.ReadLine());

                            switch (d)
                            {
                                case 0: flag1 = false; break;
                                case 1:
                                    Console.WriteLine("Enter the size of matrits:");
                                    Console.WriteLine("Enter the size of rows for the 1:");
                                    int row1 = Convert.ToInt32(Console.ReadLine());
                                    Console.WriteLine("Enter the size of columns for the 1:");
                                    int col1 = Convert.ToInt32(Console.ReadLine());
                                    double[,] matr1 = getMatr(row1, col1);
                                    
                                    Console.WriteLine("Enter the size of rows for the 2:");
                                    int row2 = Convert.ToInt32(Console.ReadLine());
                                    Console.WriteLine("Enter the size of columns for the 2:");
                                    int col2 = Convert.ToInt32(Console.ReadLine());
                                    double[,] matr2 = getMatr(row2, col2);
                                    if (col1 == row2) {
                                        Calculator calc = new Calculator();
                                        double[,] res = new double[row1,col2];
                                        res = calc.multiplyMatr(matr1, matr2, row1, col1, col2);
                                        Console.WriteLine("Result matriza: ");
                                        showMatr(res, row1, col2);
                                    }
                                    break;
                            }
                        }
                        break;
                    case 0:
                        Environment.Exit(0);
                        break;


                }
            }
     
           
        }


        private static double enterNumber() { 
            double a;
            Console.WriteLine("Enter the number:");
            a = Convert.ToDouble(Console.ReadLine());
            return a; 
        }

        private static double[,] getMatr(int row, int col)
        {
            
            double[,] matr = new double[row, col];
            for (int i = 0; i < row; i++) // заполнение
            {
                for (int j = 0; j < col; j++)
                {
                    matr[i, j] = i + 1 + j;
                }
            }
            Console.WriteLine();

            showMatr(matr, row, col);

            Console.WriteLine();

            return matr;
        }
        private static void showMatr(double[,] matr, int rows, int cols)
        {
            for(int i = 0; i < rows; i++)
            {
                Console.Write("| ");
                for (int j = 0; j < cols; j++)
                {
                    Console.Write(matr[i, j]+" ");
                }
                Console.Write("|");
                Console.WriteLine();
            }

        }
    }

}
